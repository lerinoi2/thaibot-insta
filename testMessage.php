<?php

/*
 * IMPORTANT!
 * You need https://github.com/Seldaek/monolog to run this example:
 * $ composer require monolog/monolog
 *
 * Also, if you have a 32-bit PHP build, you have to enable the GMP extension:
 * http://php.net/manual/en/book.gmp.php
 */

set_time_limit(0);
date_default_timezone_set('UTC');

require __DIR__.'/vendor/autoload.php';

/////// CONFIG ///////
$username = 'lerinoi';
$password = '4815163242LoSt';
$debug = false;
$truncatedDebug = false;
//////////////////////

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    exit(0);
}

$loop = \React\EventLoop\Factory::create();
if ($debug) {
    $logger = new \Monolog\Logger('rtc');
    $logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::INFO));
} else {
    $logger = null;
}
$rtc = new \InstagramAPI\Realtime($ig, $loop, $logger);

$rtc->on('thread-item-created', function ($threadId, $threadItemId, \InstagramAPI\Response\Model\DirectThreadItem $threadItem) {
global $ig;

    $id = $threadItem->getUserId();
    $userId = $ig->people->getInfoById($id);
    $name = $userId->getUser()->getUsername();
    $recipients =
        [
            'users' => ['users' => array($id)]
        ];
    $ig->direct->sendText( $recipients['users'], "Добрый день! Вы обратились в компанию ThaiRaiHome.");
    $ig->direct->sendText( $recipients['users'], "Чтобы узнать о нас больше прямо сейчас, вы можете посмотреть видео");
    $ig->direct->sendText( $recipients['users'], "https://youtu.be/zQ6uD-H4KUE");
    $ig->direct->sendText( $recipients['users'], "или пообщаться с нашим чат-консультантом в Вайбере");
    $ig->direct->sendText( $recipients['users'], "https://thairaihome.com/bot");
});

$rtc->on('error', function (\Exception $e) use ($rtc, $loop) {
    printf('[!!!] Got fatal error from Realtime: %s%s', $e->getMessage(), PHP_EOL);
    $rtc->stop();
    $loop->stop();
});
$rtc->start();

$loop->run();

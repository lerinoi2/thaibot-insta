<?php
$pid = pcntl_fork();
if ($pid == -1) {
    die('could not fork'.PHP_EOL);
} else if ($pid) {
    die('die parent process'.PHP_EOL);
} else {
    posix_setsid();
    $baseDir = dirname(__FILE__);
    ini_set('error_log',$baseDir.'/error.log');
    fclose(STDIN);
    fclose(STDOUT);
    fclose(STDERR);
    $STDIN = fopen('/dev/null', 'r');
    $STDOUT = fopen($baseDir.'/application.log', 'ab');
    $STDERR = fopen($baseDir.'/daemon.log', 'ab');
        while(true) {
            require __DIR__.'/vendor/autoload.php';
            include_once 'bd.php';
            set_time_limit(0);
            date_default_timezone_set('UTC');

            /////// CONFIG ///////
            $username = 'Thairaihome';
            $password = '28567Phuket';
            $debug = false;
            $truncatedDebug = false;
            $recipients = '';
            $name = '';
            //////////////////////

            $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
            $bd = new Thai();
            try {
                $ig->login($username, $password);
            } catch (\Exception $e) {
                echo 'Something went wrong: '.$e->getMessage()."\n";
            }
            $loop = \React\EventLoop\Factory::create();
            if ($debug) {
                $logger = new \Monolog\Logger('rtc');
                $logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::INFO));
            } else {
                $logger = null;
            }
            $rtc = new \InstagramAPI\Realtime($ig, $loop, $logger);

            $rtc->on('thread-item-created', function ($threadId, $threadItemId, \InstagramAPI\Response\Model\DirectThreadItem $threadItem) {
                global $ig, $bd;
                $id = $threadItem->getUserId();
                $userId = $ig->people->getInfoById($id);
                $name = $userId->getUser()->getUsername();
                $recipients =
                    [
                        'users' => ['users' => array($id)]
                    ];

                $bd->addUser($id, $name);

                $res = $bd->setSend($recipients['users']['users'][0]);

                if ($res != 0) {

                    $ig->direct->sendText( $recipients['users'], "Добрый день! Вы обратились в компанию ThaiRaiHome.");
                    $ig->direct->sendText( $recipients['users'], "Чтобы узнать о нас больше прямо сейчас, вы можете посмотреть видео");
                    $ig->direct->sendText( $recipients['users'], "https://youtu.be/zQ6uD-H4KUE");
                    $ig->direct->sendText( $recipients['users'], "или пообщаться с нашим чат-консультантом в Вайбере");
                    $ig->direct->sendText( $recipients['users'], "https://thairaihome.com/bot");

                    $bd->finals($threadItem->getText(), $name);
                }
            });

            $rtc->start();
            $loop->run();
        }
}
?>

<?php

class Thai
{
    private  $host = 'localhost:3306';
    private  $login = 'phpmyadmin';
    private  $password = 'some_pass';
    private  $charset = 'utf8';
    private  $connection;

    function get_Data_DB ($query, $dbname = 'thai')
    {
        $this->connection = "mysql:host=$this->host;dbname=$dbname;charset=$this->charset";
        try
        {
            $db = new PDO($this->connection, $this->login, $this->password);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
        $stmt = $db->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $out[] = $row;
        }
        return $out ? $out : false;
    }

    public function addUser($chatId, $name)
    {
        $res = $this->get_Data_DB("SELECT `name` FROM userInst WHERE `chat_id` = '".$chatId."'")[0]['name'];
        if (is_null($res)){
            $this->get_Data_DB("INSERT INTO `userInst` (`chat_id`, `name`) VALUES ('".$chatId."', '".$name."')");
        }
    }

    public function setSend($chatId)
    {
        $res = $this->get_Data_DB("SELECT `send` FROM userInst WHERE `chat_id` = '".$chatId."'")[0]['send'];

        $this->get_Data_DB("UPDATE `userinst` SET send = 0 WHERE `chat_id` = '".$chatId."'");

        if (is_null($res) or $res == ""){
            $res = 1;
        }

        return $res;
    }

    public function setNumber($chatId, $number){
        $this->get_Data_DB("UPDATE `userinst` SET number = '".$number."' WHERE `chat_id` = '".$chatId."'");
    }

    public function setName($chatId, $name){
        $this->get_Data_DB("UPDATE `userinst` SET username = '".$name."' WHERE `chat_id` = '".$chatId."'");
    }

    public function getNumber($chatId){
        return $this->get_Data_DB("SELECT `number` FROM userInst WHERE `chat_id` = '".$chatId."'")[0]['number'];
    }

    public function sendAdmin($info){

        $chatId = $this->get_Data_DB("select `chatid` from `admin` where `check` = '1'");

        foreach ($chatId as $value){
            foreach ($value as $item) {
                $chats[]= $item;
            }
        }

        $data = [
            'send' => '1',
            'chatid' => json_encode($chats),
            'data' => json_encode($info),
        ];

        $options = [
            CURLOPT_URL => 'https://bot.delive.me/thai/viber/admin/bot.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
        ];

        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }

    public function setMessage($chatId, $text){
        $this->get_Data_DB("UPDATE `userinst` SET message = '".$text."' WHERE `chat_id` = '".$chatId."'");
    }
    public function getMessage($chatId){
        return $this->get_Data_DB("SELECT `message` FROM userInst WHERE `chat_id` = '".$chatId."'")[0]['message'];
    }
    public function setEmail($chatId, $text){
        $this->get_Data_DB("UPDATE `userinst` SET email = '".$text."' WHERE `chat_id` = '".$chatId."'");
    }
    public function getUsername($chatId){
        return $this->get_Data_DB("SELECT `username`, `name` FROM userInst WHERE `chat_id` = '".$chatId."'")[0];
    }
    
    public function finals($text, $name){
        
        $arData = [
            'Заявка' => 'инстаграм сообщение в директ',
            'ник' => $name,
            'Текст сообщения' => $text,
        ];
        $this->sendAdmin($arData);
    }
    
    public function finalCom($recipients, $userId, $nick, $texts){
        $this->setSend($recipients['users']['users'][0]);

        $this->addUser($userId, $nick);

        unset($texts[0]);
        unset($texts[1]);

        $comment = implode(" ", $texts);

        $arData =
            [
                'Заявка' => 'инстаграм коммент',
                'ник' => $nick,
                'текст комментария' => $comment,
            ];
        $this->sendAdmin($arData);

    }
}
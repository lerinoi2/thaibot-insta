<?php
$pid = pcntl_fork();
if ($pid == -1) {
    die('could not fork'.PHP_EOL);
} else if ($pid) {
    die('die parent process'.PHP_EOL);
} else {
    posix_setsid();
    $baseDir = dirname(__FILE__);
    ini_set('error_log', $baseDir . '/error.log');
    fclose(STDIN);
    fclose(STDOUT);
    fclose(STDERR);
    $STDIN = fopen('/dev/null', 'r');
    $STDOUT = fopen($baseDir . '/application.log', 'ab');
    $STDERR = fopen($baseDir . '/daemon.log', 'ab');
    // Новый процесс, запускаем главный цикл
    while (true) {
        set_time_limit(0);
        date_default_timezone_set('UTC');

        require __DIR__ . '/vendor/autoload.php';
        include_once 'bd.php';

        /////// CONFIG ///////
        $username = 'Thairaihome';
        $password = '28567Phuket';
        $debug = false;
        $truncatedDebug = false;
        //////////////////////

        $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
        $bd = new Thai();

        try {
            $ig->login($username, $password);
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
            exit(0);
        }

        $loop = \React\EventLoop\Factory::create();
        if ($debug) {
            $logger = new \Monolog\Logger('push');
            $logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::INFO));
        } else {
            $logger = null;
        }
        $push = new \InstagramAPI\Push($loop, $ig, $logger);

        $push->on('comment', function (\InstagramAPI\Push\Notification $push) {

            global $ig, $bd;
            $message = $push->getMessage();
            $texts = explode(" ", $message);
            $nick = $texts[0];
            $userId = $ig->people->getUserIdForName($nick);
            $recipients = "";

            $recipients =
                [
                    'users' => ['users' => array($userId)]
                ];

            $ig->direct->sendText( $recipients['users'], "Добрый день! Вы обратились в компанию ThaiRaiHome.");
            $ig->direct->sendText( $recipients['users'], "Чтобы узнать о нас больше прямо сейчас, вы можете посмотреть видео");
            $ig->direct->sendText( $recipients['users'], "https://youtu.be/zQ6uD-H4KUE");
            $ig->direct->sendText( $recipients['users'], "или пообщаться с нашим чат-консультантом в Вайбере");
            $ig->direct->sendText( $recipients['users'], "https://thairaihome.com/bot");

            $bd->finalCom($recipients, $userId, $nick, $texts);

        });

        $push->start();

        $loop->run();
    }
}

?>